<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
})->name('welcome');

Route::get('/home', 'HomeController@index')->name('home');

Route::name('auth.resend_confirmation')->get('/register/confirm/resend', 'Auth\RegisterController@resendConfirmation');
Route::name('auth.confirm')->get('/register/confirm/{confirmation_code}', 'Auth\RegisterController@confirm');

Auth::routes();

Route::get('/avatar/{username}', 'AvatarController@view')->name('avatar.view');

Route::post('/question/destroy/{question}', 'QuestionsController@destroy')->name('questions.destroy');
Route::post('/question/flag/{question}', 'QuestionsController@flag')->name('questions.flag');
Route::post('/question/block/user/{question}', 'QuestionsController@block')->name('questions.block');

Route::get('/{username}', 'QuestionsController@index')->name('questions.index');
Route::post('/{username}', 'QuestionsController@store')->name('questions.store');

Route::get('/{username}/question/{question}', 'QuestionsController@show')->name('questions.show');
Route::post('/{username}/question/', 'QuestionsController@answer')->name('questions.answer');

