<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConfirmationFieldsToUsers extends Migration
{
	/**
	 * Run the migrations.
	 */
	public function up()
	{
		Schema::table('users', function (Blueprint $table) {
			$table->dateTime('confirmed_at')->nullable();
			$table->string('confirmation_code')->nullable();
		});

		DB::table('users')->insert( array ([
				'username' => 'serge',
				'email' => 'serge.benard@gmail.com',
				'emailEncoded' => Hash::make('serge.benard@gmail.com'),
				'password' => Hash::make('testing'),
				'created_at' => date('Y-m-d H:i:s'), //'2018-07-30 21:17:03'
				'updated_at' => date('Y-m-d H:i:s'), //'2018-07-30 21:17:03'
				'confirmed_at' => date('Y-m-d H:i:s'), //'2018-07-30 21:17:03'
			],
			[
				'username' => 'admin',
				'email' => 'admin@curiouz.com',
				'emailEncoded' => Hash::make('admin@curiouz.com'),
				'password' => Hash::make('testing'),
				'created_at' => date('Y-m-d H:i:s'), //'2018-07-30 21:17:03'
				'updated_at' => date('Y-m-d H:i:s'), //'2018-07-30 21:17:03'
				'confirmed_at' => date('Y-m-d H:i:s'), //'2018-07-30 21:17:03'
			])
		);
	}

	/**
	 * Reverse the migrations.
	 */
	public function down()
	{
		Schema::table('users', function (Blueprint $table) {
			$table->dropColumn('confirmed_at');
			$table->dropColumn('confirmation_code');
		});
	}
}
