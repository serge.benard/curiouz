@extends('layouts.app')

@section('content')
	<div class="min-h-screen flex items-center justify-center">
		<h1 class="text-5xl text-purple font-sans my-4 block">Oops!</h1>
		<div class="inline">
			<p class="block w-full">{{ $exception->getMessage() }}</p>
		</div>
	</div>
@endsection
