
<svg width="24px" height="24px" viewBox="0 0 24 24" @if ( $class ) class="{{ $class  }}" @endif xmlns="http://www.w3.org/2000/svg">
    <!-- Generator: Sketch 40.1 (33804) - http://www.bohemiancoding.com/sketch -->
    
    
    <defs></defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
        <g id="houndstooth">
            <polygon id="Rectangle-20" points="1.99840144e-15 18 6 18 12 12 12 18 18 18 12 24 3.99680289e-15 24"></polygon>
            <polygon id="Rectangle-20-Copy" points="24 18 24 24 18 24"></polygon>
            <polygon id="Rectangle-20-Copy-2" points="24 1.77635684e-15 18 6 12 6 18 1.77635684e-15"></polygon>
            <polygon id="Rectangle-20-Copy-3" points="12 -4.08562073e-14 12 6 0 18 1.99840144e-15 12 6 6 1.99840144e-15 6 1.99840144e-15 1.11022302e-15"></polygon>
        </g>
    </g>
</svg>
