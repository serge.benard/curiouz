
<svg width="40px" height="1px" viewBox="0 0 40 1" @if ( $class ) class="{{ $class  }}" @endif xmlns="http://www.w3.org/2000/svg">
    <!-- Generator: Sketch 40.1 (33804) - http://www.bohemiancoding.com/sketch -->
    
    
    <defs></defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
        <g id="stripes">
            <rect id="Rectangle-27" x="0" y="0" width="20" height="1"></rect>
        </g>
    </g>
</svg>