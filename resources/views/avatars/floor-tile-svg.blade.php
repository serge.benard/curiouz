
<svg width="30px" height="30px" viewBox="0 0 30 30" @if ( $class ) class="{{ $class  }}" @endif xmlns="http://www.w3.org/2000/svg">
    <!-- Generator: Sketch 40 (33762) - http://www.bohemiancoding.com/sketch -->
    
    
    <defs></defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
        <g id="floor-tile">
            <path d="M0,10 L10,10 L10,20 L0,20 L0,10 Z M10,0 L20,0 L20,10 L10,10 L10,0 Z" id="Combined-Shape"></path>
        </g>
    </g>
</svg>