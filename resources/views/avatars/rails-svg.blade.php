
<svg width="20px" height="10px" viewBox="0 0 20 10" @if ( $class ) class="{{ $class  }}" @endif xmlns="http://www.w3.org/2000/svg">
    <!-- Generator: Sketch 40.1 (33804) - http://www.bohemiancoding.com/sketch -->
    
    
    <defs></defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
        <g id="rails">
            <path d="M16,6 L6,6 L6,10 L4,10 L4,6 L2,6 L2,4 L4,4 L4,0 L6,0 L6,4 L16,4 L16,0 L18,0 L18,4 L20,4 L20,6 L18,6 L18,10 L16,10 L16,6 Z" id="Combined-Shape"></path>
        </g>
    </g>
</svg>