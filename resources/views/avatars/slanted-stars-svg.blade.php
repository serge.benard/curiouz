
<svg width="30px" height="30px" viewBox="0 0 30 30" @if ( $class ) class="{{ $class  }}" @endif xmlns="http://www.w3.org/2000/svg">
    <!-- Generator: Sketch 40 (33762) - http://www.bohemiancoding.com/sketch -->
    
    
    <defs></defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
        <g id="triangles">
            <path d="M0,15 L15,30 L0,30 L0,15 L0,15 Z M15,0 L30,15 L30,0 L15,0 L15,0 Z" id="Combined-Shape"></path>
        </g>
    </g>
</svg>
