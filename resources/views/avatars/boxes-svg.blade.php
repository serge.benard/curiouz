
<svg width="20px" height="20px" viewBox="0 0 20 20" @if ( $class ) class="{{ $class  }}" @endif xmlns="http://www.w3.org/2000/svg">
    <!-- Generator: Sketch 40 (33762) - http://www.bohemiancoding.com/sketch -->
    
    
    <defs></defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
        <g id="bozes">
            <polygon id="Rectangle-182-Copy" points="0 0 20 0 0 20"></polygon>
        </g>
    </g>
</svg>