
<svg width="20px" height="20px" viewBox="0 0 20 20" @if ( $class ) class="{{ $class  }}" @endif xmlns="http://www.w3.org/2000/svg">
    <!-- Generator: Sketch 40 (33762) - http://www.bohemiancoding.com/sketch -->
    
    
    <defs></defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
        <g id="dots">
            <circle id="Oval-377-Copy-9" cx="3" cy="3" r="3"></circle>
            <circle id="Oval-377-Copy-14" cx="13" cy="13" r="3"></circle>
        </g>
    </g>
</svg>