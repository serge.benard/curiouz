
<svg width="40px" height="40px" viewBox="0 0 40 40" @if ( $class ) class="{{ $class  }}" @endif xmlns="http://www.w3.org/2000/svg">
    <!-- Generator: Sketch 40.1 (33804) - http://www.bohemiancoding.com/sketch -->
    
    
    <defs></defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
        <g id="diagonal-stripes">
            <polygon id="Path-2" points="0 40 40 0 20 0 0 20"></polygon>
            <polygon id="Path-2-Copy" points="40 40 40 20 20 40"></polygon>
        </g>
    </g>
</svg>