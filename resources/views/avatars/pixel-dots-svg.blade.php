
<svg width="16px" height="16px" viewBox="0 0 16 16" @if ( $class ) class="{{ $class  }}" @endif xmlns="http://www.w3.org/2000/svg">
    <!-- Generator: Sketch 40 (33762) - http://www.bohemiancoding.com/sketch -->
    
    
    <defs></defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
        <g id="pixel-dots">
            <path d="M0,4.16333634e-16 L16,4.16333634e-16 L16,2 L10,2 L10,8 L16,8 L16,16 L8,16 L8,10 L2,10 L2,16 L0,16 L0,4.16333634e-16 Z M4,4 L6,4 L6,6 L4,6 L4,4 Z M12,12 L14,12 L14,14 L12,14 L12,12 Z M4,12 L6,12 L6,14 L4,14 L4,12 Z M12,4 L14,4 L14,6 L12,6 L12,4 Z" id="Combined-Shape"></path>
        </g>
    </g>
</svg>