@extends('layouts.app')

@section('page-head')
	<!-- For Google -->
	<meta name="description" content="Ask a question {{ $user->username }} a question or confess something why don't you!" />
	<meta name="keywords" content="request ask confess list fun funny respect respectful embarrassing question {{ $user->username }} people friend acquaintance get to know silly" />

	<meta name="author" content="{{ $user->username }}, via {{ Request::url() }}" />
	<meta name="copyright" content="All of these questions are owned by {{ $user->username }}. Site design and ideas are property of {{ config('app.name', 'Curiouz') }}" />
	<meta name="application-name" content="{{ config('app.name', 'Curiouz') }}" />

	<!-- For Facebook -->
	<meta property="og:title" content="Ask {{ $user->username }} a question or confess something why don't you!" />
	<meta property="og:type" content="app" />
	<meta property="og:image" content="{{ route('avatar.view', $user->username) }}" />
	<meta property="og:url" content="{{ Request::url() }}" />
	<meta property="og:description" content="Whatever you can choose to share with {{ $user->username }} can be done anonymously!" />

	<!-- For Twitter -->
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:title" content="Ask {{ $user->username }} a question or confess something why don't you!" />
	<meta name="twitter:description" content="Whatever you can choose to share with {{ $user->username }} can be done anonymously! {{ Request::url() }}" />
	<meta name="twitter:image" content="{{ route('avatar.view', $user->username) }}" />
@endsection

@section('sidebar')
	@if( Auth::id() === $user->id )
	@include( 'partials.left-column.avatar', ['greeting' => 'Hello, '] )
	@else
	@include( 'partials.left-column.avatar', ['greeting' => 'Here\'s '] )
	@endif
@endsection

@section('content')

	@include( 'partials.alerts' )

	{{-- If successfully submitted question, don't show the form but
		a link to allow to submit another question --}}
	@if( !Session::has('success') )
	@include( 'partials.left-column.question-form' )
	@endif

	<div class="w-full p-0 sm:p-4 block h-full">
		@if( count( $questions ) === 0 )
		<div class="text-center">
			@if( Auth::id() === $user->id )
			<h2 class="text-3xl block text-blue">There's nothing to show you yet!</h2>
			<div class="p-8 text-center text-xl block">
					Why don't you let your friends know you want to be less mysterious?
				<div class="pt-8 block mx-auto">
					<a	href="https://twitter.com/intent/tweet?text={{ htmlspecialchars('Ask me a question or confess something, why don\'t you. ') . url()->current() }}"
							target="new"
							class="rounded text-xs hover:bg-transparent bg-blue border border-blue hover:text-blue text-blue-lightest p-2 mx-2 no-underline">
						Tweet It At 'Em
					</a>
					<a	href="https://www.facebook.com/sharer/sharer.php?caption={{ htmlspecialchars('Ask me a question or confess something, why don\'t you.') }}&description={{ htmlspecialchars('You can choose to stay anonymous if you want!') }}&u={{ url()->current() }}"
							target="new"
							class="rounded text-xs hover:bg-transparent bg-blue border border-blue hover:text-blue text-blue-lightest p-2 mx-2 no-underline">
						FaceBook Share It
					</a>
				</div>
			</div>
			@else
			Nothing to show for {{ $user->username }}!
			<div class="w-full block p-4 text-center text-xl">
				Why don't you ask them a question, or maybe even confess something?
			</div>
			@endif
		</div>

		@else
		@if( $questions->where('answer', null)->count() )
		<div class="w-full block shadow bg-grey-lightest py-4 mb-6">


			<div class="mb-2 px-4">

				<h2 class="relative inline-block pr-6">
					Without Replies
					@if( $questions->where('answer', null)->count() )
					<i class="absolute block pin-t pin-r text-xs text-center rounded-full bg-blue text-blue-lightest px-2 py-1 leading-none whitespace-no-wrap min-w-5 -mt-2">
						{{ $questions->where('answer', null)->count() }}
					</i>
					@endif
				</h2>

			</div>

			<form method="POST" action="{{ route('questions.answer', [ $user->username ]) }}">
				@php
				$i = 0;
				@endphp
			@foreach( $questions->where('answer', null) as $question )
				@csrf
				<div class="w-full block {{ $i % 2 == 0 && $loop->count != 1 ? 'bg-white': 'bg-transparent' }} @if( $loop->first ) pt-4 pb-3 @elseif( $loop->last ) py-4 @else py-4 @endif @if( $errors->has('answer.' .$i) ) border border-red bg-red-lightest text-red @endif">

					<div class="flex px-4">

						@empty( $question->anonymous )
						@include( 'avatars/' . $user->avatar , [ 'class' => 'w-8 h-8 text-blue fill-current rounded-full border-2 border-blue flex-none mr-2' ] )
						@else
						@include( 'avatars/anonymous-svg', [ 'class' => 'w-8 h-8 text-blue fill-current rounded-full border-2 border-blue flex-none mr-2' ] )
						@endif

						<div class="flex-none">
							<div class="w-auto text-grey-dark text-sm">
								sent {{ $question->created_at->diffForHumans() }}
							</div>

							<div class="sm:pr-5">
								<span class="font-semibold">
								@if( $question->anonymous !== 1 )
									<a class="text-blue" href="#">{{ $question->asking->username }}</a>
								@else
									Anonymous
								@endif

							</span> sez:

							</div>
						</div>

					</div>

					<div class="w-full block px-4 pt-2 pb-4">
						{{ $question->text }}
					</div>

					<label class="block w-full font-semibold pb-1  px-4 @if( $errors->has('answer.' .$i) ) text-red @endif" for="answer.{{ $question->id }}">
						Reply
					</label>

					<div class="block px-4 pb-2">
						<textarea class="block w-full h-16 shadow border border-grey bg-white @if( $errors->has('answer.' .$i) ) border border-red bg-red-lightest @endif" name="answer[]" id="answer.{{ $question->id }}">{{ old('answer.' .$i) }}</textarea>
					</div>
					<input type="hidden" name="question_id[]" id="id.{{ $question->id }}" value="{{ $question->id }}">

					<div class="block w-full p-2 sm:p-4">
						<div class="flex flex-wrap sm:flex-no-wrap">
							<button class="block w-full sm:flex-1 rounded text-center text-xs font-semibold p-2 border border-grey-light bg-transparent text-grey-light hover:text-blue-lightest hover:bg-blue hover:border-blue"
								type="submit">
								Save All Replies
							</button>
							<label
								class="flex justify-center items-center w-full sm:flex-none sm:w-auto rounded text-xs font-semibold p-2 border border-grey-light bg-transparent text-grey-light hover:text-yellow-lightest hover:bg-red hover:border-red sm:mx-2 my-2 sm:my-0">

								<div class="checkboxToggle w-4 h-4 bg-white rounded-full relative shadow py-2 mr-2">
									<input
										class="hidden"
										type="checkbox"
										name="selector[]"
										id="selector.{{ old('answer.' .$i) }}"
										value="1"
										@if( old('selector.' .$i) !== null && old('selector.' .$i) !== 0 ) checked @endif>
									<div class="checkboxToggleCenter w-3 h-3 rounded-full absolute z-1 shadow-inner" for="selector"></div>
								</div>
								Block
							</label>
						</div>
					</div>
				</div>
				@php
				$i++
				@endphp
			@endforeach
				<div class="block p-4 bg-white border border-grey-light">
					<button
						class="block w-full rounded text-sm font-semibold p-2 border border-blue bg-blue text-blue-lightest hover:text-blue hover:bg-blue-lightest"
						type="submit">
						Save All Replies
					</button>
				</div>
			</form>
		</div>
		@endif

		@if( $questions->where('answer', '!=', null)->count() )
		<div class="w-full block shadow bg-grey-lightest py-4 mb-6">
			<div class="block pt-4">

					<h2 class="relative inline-block pr-8">
						With Replies
						@if( $questions->where('answer', '!=', null)->count() )
						<i class="absolute block pin-t pin-r text-xs text-center rounded-full bg-blue text-blue-lightest px-3 py-2 leading-none whitespace-no-wrap min-w-5 -mt-3">
							{{ $questions->where('answer', '!=', null)->count() }}
						</i>
						@endif
					</h2>
			</div>

			@foreach( $questions->where('answer', '!=', null ) as $question )
			<div class="w-full">
				@if( $question->anonymous === 1 )
				Replied
				@else
				{{ $question->asking->username }}
				@endif
				asks:
			</div>
			<div class="w-full">
				{{ $question->text }}
			</div>
			<div class="w-full">
				{{ $question->answer }}
			</div>
			@endforeach
		</div>
		{{-- End of if there are replies --}}
		@endif
		{{-- End of if there are questions returned --}}
		@endif

	</div>
@endsection
