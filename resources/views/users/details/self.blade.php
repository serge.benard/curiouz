@extends('layouts.app')

@section('sidebar')
	@include( 'partials.question-form.self' )
@endsection

@section('content')

	<div class="w-full block">
		@if( count( $questions ) === 0 )
		<div>
			No questions yet!
		</div>
		@else
		<div class="w-full">
			@if( !empty($questions->where('answer', null) ) && $questions->where('answer', null)->count() !== 0 )
			<h2 class="w-full">
				Unanswered
					{{ $questions->where('answer', null)->count() }}
			</h2>

			@foreach( $questions->where('answer', null) as $question )
			<form method="POST" action="{{ route('questions.answer', [ $user->username, $question->id ]) }}">
				@csrf
				<div class="w-full">
					@if( $question->anonymous === 1 )
					Anonymous
					@else
					{{ $user->username }}
					@endif
					asks:
				</div>
				<div class="w-full">
					{{ $question->text }}
				</div>
				<label class="w-full block" for="answer{{ $question->id }}">Answer</label>
				<textarea class="block" name="answer" id="answer{{ $question->id }}" required>{{ old('answer') }}</textarea>
				<button type="submit">Save Answer</button>
			</form>
			@endforeach

			@endif
		</div>

		<div class="w-full">
			<h2 class="w-full">
				Answered
				@if( !empty($questions->where('answer', '!=', null ) ) )
					{{ $questions->where('answer', '!=', null )->count() }}
				@endif
			</h2>

			@foreach( $questions->where('answer', '!=', null ) as $question )
			<div class="w-full">
				@if( $question->anonymous === 1 )
				Anonymous
				@else
				{{ $user->username }}
				@endif
				asks:
			</div>
			<div class="w-full">
				{{ $question->text }}
			</div>
			<div class="w-full">
				{{ $question->answer }}
			</div>
			@endforeach
		</div>
		@endif

	</div>
@endsection
