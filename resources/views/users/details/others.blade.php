@extends('layouts.app')

@section('content')

	@include( 'partials.question-form.others' )

	<div>
		@if( count( $questions ) === 0 )
		<div>
			No questions yet!
		</div>
		@else
		<div class="w-full">
			<h2 class="w-full">
				Unanswered
				@unless($questions->where('answer', null))
					{{ $questions->where('answer', null)->count() }}
				@endif
			</h2>
			@foreach( $questions->where('answer', null) as $question )
			<div class="w-full">
				{{ $question->text }}
			</div>
			@endforeach
		</div>
		@endif

	</div>
@endsection
