<div>
	<div class="w-full">
		About {{ $user->username }}.
	</div>
	<div class="w-full">
		{{ $user->biography }}
	</div>
	<form action="{{ route('questions.store', [ $user->username ]) }}" method="post">
		@csrf
		<label class="w-full" for="text">Question</label>
		<textarea class="block" name="text" id="text" cols="30" rows="10" required>{{ old('text') }}</textarea>
		<div class="w-full">
			<label for="anonymous">Anonymous?</label>
			<input type="checkbox" name="anonymous" id="anonymous" value="1" @if( old('anonymous') === 1 ) checked @endif>
		</div>
		<button class="w-full" type="submit">Ask Question</button>
	</form>
</div>
