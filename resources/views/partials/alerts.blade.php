	@if ($errors->any() || Session::get('login_error'))
	<div class="bg-red-lightest border border-red-light text-red-dark px-4 py-3 rounded mx-4 mb-6 shadow-md" role="alert">
		<strong class="block font-bold">Uh-oh, Whooops!</strong>
		<span class="block py-3 font-thin">Something's not right with your submission. Here are the culprits:</span>

		@if( $errors->any() )
			@if( $errors->has( 'answer.*' ) )

			<p class="pl-4 block py-2 text-sm font-semibold">
				It looks like one or more replies are not long enough!
			</p>

			@else

			<ul class="list-reset pl-4 text-sm font-semibold">
				@foreach ($errors->all() as $error)
					<li class="block pb-1">{{ $error }}</li>
				@endforeach
			</ul>

			@endif

		@endif

		@if( Session::has('login_error') )
			<p class="pl-4 text-sm font-semibold">
				{{ Session::get('login_error') }}
			</p>
		@endif

		<span class="block py-2 font-thin">Let's fix these and get on our way!</span>

	</div>

	@elseif( Session::has('success') )

	<div class="bg-green-lightest border border-green text-green-dark px-4 py-3 rounded mx-4 my-2 shadow-md" role="alert">
		<strong class="font-bold block">WAOOH, WOOP WOOP!</strong>
		@if( Session::get('success') == 'question' )
		<span class="block py-3 font-thin">
			You sent us something, and we caught it!
		</span>

		<a
			class="block text-center rounded border text-green text-sm font-bold border-green p-2 bg-transparent hover:text-green-lightest hover:bg-green no-underline my-4 mx-2"
			href="#">Do you want to know what happens next?</a>

		<span class="block py-2 md:inline font-thin mt-4">
			You can also
			<a
				class="text-green font-bold"
				href="{{ route('questions.index', $user->username) }}">send something else</a>!
		</span>

		@elseif( Session::get('success') == 'answer' )
		<span class="block py-3 font-thin">
			You sent us one or more replies, and we caught em!
		</span>
		@endif
	</div>

	@elseif( Session::has('message') )

	<div class="bg-blue-lightest border border-blue text-blue-dark px-4 py-3 rounded mx-4 my-2 shadow-md" role="alert">
		<strong class="font-bold">Hmm, what do we have here?</strong>
		<span class="block py-2 font-thin">
			{{ Session::get('message', 'Not sure what happened, but something ain\'t right.') }}
		</span>
	</div>
	@endif
