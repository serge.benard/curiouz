@if( Auth::id() !== $user->id )
	<div class="w-full sm:mt-2 sm:shadow-lg bg-grey-lighter p-4 sm:p-8">

		<form	action="{{ route('questions.store', [ $user->username ]) }}"
				method="post">

			@csrf

			<label class="w-full pb-2 block @if( $errors->has('text') ) text-red @endif" for="text">
				Ask a Question or Confess Something to
				<span class="font-semibold">
					{{ $user->username }}
				</span>
			</label>

			<textarea class="block w-full h-16 sm:h-24 p-1 shadow border border-transparent mb-1 @if( $errors->has('text') ) border-red bg-red-lightest @endif" name="text" id="text">{{ old('text') }}</textarea>

			<p class="block w-full pb-4 text-sm @if( $errors->has('text') )text-red @else font-thin @endif">
				This text box has to have at least 3 characters in it.
			</p>

			@if( !Auth::check() )
			<div class="w-full p-2 @if( $errors->has('email') || $errors->has('password') || Session::get('login_error') ) bg-red-lighter @else bg-grey-light @endif">

				<p class="w-full pb-2 font-semibold @if( $errors->has('email') || $errors->has('password') || Session::get('login_error') ) text-red @else text-blue @endif">
					You need to log in to interact with {{ $user->username }}.
				</p>

				<label class="w-full block pt-2 pb-1 @if( $errors->has('email') || Session::get('login_error') ) text-red @else font-semibold @endif" for="email">
					Email
				</label>

				<input
					class="w-full md:w-1/3 block p-1 shadow @if( $errors->has('email') || Session::get('login_error') ) border border-red bg-red-lightest @endif"
					type="text"
					name="email"
					id="email"
					value="{{ old('email') }}"
					required>
				@if( $errors->has('email') )
				<p class="block w-full pt-1 pb-4 text-sm text-red">
					{{ $errors->first('email') }}
				</p>
				@endif

				<label
					class="w-full block pt-2 pb-1 @if( $errors->has('password') || Session::get('login_error') ) text-red @else font-semibold @endif"
					for="password">
					Password
				</label>

				<input
					class="w-full md:w-1/3 block p-1 shadow @if( $errors->has('password') || Session::get('login_error') ) border border-red bg-red-lightest @endif"
					type="password"
					name="password"
					id="password"
					value=""
					required>

				@if( $errors->has('password') )
				<p class="block w-full pt-1 pb-4 text-sm text-red">
					{{ $errors->first('password') }}
				</p>
				@endif
			</div>
			@endif

			<label
				class="py-2 flex"
				for="anonymous">

				Stay Anonymous?

				<div class="checkboxToggle w-4 h-4 bg-white rounded-full relative shadow py-2 ml-2">
					<input
						class="hidden"
						type="checkbox"
						name="anonymous"
						id="anonymous"
						value="1"
						@if( old('anonymous') !== null && old('anonymous') !== 0 ) checked @endif>
					<div class="checkboxToggleCenter w-3 h-3 rounded-full absolute z-1 shadow-inner" for="anonymous"></div>
				</div>
			</label>

			<button class="w-full sm:w-auto p-2 rounded border-2 border-blue bg-transparent hover:bg-blue text-blue hover:text-blue-lightest my-2 shadow-md" type="submit">Ask or Confess It!</button>
		</form>

	</div>

@endif
