<div class="w-full">
	<div class="avatar-holder text-blue text-center mb-4">
		@empty( $user->avatar )
		@include( 'avatars/anonymous-svg', [ 'class' => 'fill-current rounded-full border-4 border-blue w-32 h-32' ] )
		@else
		@include( 'avatars/' . $user->avatar, [ 'class' => 'fill-current rounded-full border-4 border-blue w-32 h-32' ] )
		@endif
	</div>
	<div class="w-full text-xl text-center my-4">
		{{ $greeting }}
		<span class="font-semibold">{{ $user->username }}</span>!
	</div>
	<div class="w-full my-4">
		@if( Auth::check() && Auth::id() === $user->id )
		<textarea class="w-full h-32" name="biography" id="biography">{{ $user->biography }}</textarea>
		@else
		{{ $user->biography }}
		@endif

	</div>
</div>
