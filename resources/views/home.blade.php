@extends('layouts.app')

@section('content')
	<div class="min-h-screen flex items-center justify-center">
		<h1 class="text-5xl text-purple font-sans">Greetings.</h1>
	</div>
	<form action="{{ route('logout') }}" method="post">
		@csrf()
		<button type="submit">Logout</button>
	</form>
@endsection
