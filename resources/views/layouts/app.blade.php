<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="{{ mix('css/app.css') }}">

	@isset( $user->username )

	@else

	@endif

	@yield('page-head')

</head>
<body class="relative font-sans">
	<div class="relative">

		<header class="absolute pin-t w-full">
			<div class="flex flex-row items-center">
				<div class="w-full sm:w-48 text-center">
					<div class="p-2 text-2xl">
					@if( Auth::check() )
						<a href="{{ route('home') }}">
					@else
						<a href="{{ route('welcome') }}">
					@endif
							{{ config('app.name', 'Curiouz') }}
						</a>
					</div>
				</div>
				<nav class="flex-auto p-2">
					<form action="{{ route('logout') }}" method="post">
						@csrf()
						<button class="rounded border-blue border p-2 text-blue text-xs" type="submit">
							Logout
						</button>
					</form>
				</nav>
			</div>
		</header>

		<div id="app" class="flex flex-col sm:flex-row h-full">
			<div class="w-full sm:w-48 bg-grey-lighter px-4 pt-16 sm:shadow-inner">
				@yield('sidebar')
			</div>
			<div class="flex-auto sm:mt-16">
				@yield('content')
			</div>
		</div>
		<div class="footer bg-grey-lighter absolute pin-x pin-b">
			testing
		</div>
	</div>
	{{-- <script src="{{ mix('js/app.js') }}"></script> --}}
</body>
</html>
