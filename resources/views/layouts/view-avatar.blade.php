<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet href="{{ asset('/css/svg.css') }}" type="text/css"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
  "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
@if( is_null($avatar) )
@include('avatars.anonymous-svg', compact('class') )
@else
@include("avatars.$avatar", compact('class') )
@endif
