<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Silber\Bouncer\Database\HasRolesAndAbilities;

class User extends Authenticatable
{
	use Notifiable;
	use HasRolesAndAbilities;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'username', 'email', 'emailEncoded', 'password',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	public function all_questions_by_user()
	{
		return $this->hasMany('App\Questions', 'asking_id', 'id' );
	}

	public function all_questions_for_user()
	{
		return $this->hasMany( 'App\Questions', 'asked_id', 'id' );
	}


}
