<?php

namespace App\Http\Controllers;

// use Session;
use App\User;
use App\Questions;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuestionsController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index( String $username, Request $request )
	{
		//
		$user = $this->validateUsername( $username );

		if ( $user->count() >= 0 ) {

			// If user is authenticated
			if ( Auth::check() )
			{
				// If logged in user is on their own page
				$auth_id = Auth::id();

				if ( $user->id === $auth_id )
				{
					$questions = 	Questions::where('asked_id', $auth_id)->
									with('asking')->
									orderBy('updated_at', 'DESC')->
									get();

					return view( 'users.details', compact( 'user', 'questions' ) );
				}
				else
				{
					$questions = 	Questions::where('asked_id', $user->id)->
									with(['asked', 'asking'])->
									whereNotNull('answer')->
									orderBy('updated_at', 'DESC')->
									get();
					return view( 'users.details', compact( 'user', 'questions' ) );
				}
			}

			$questions = 	Questions::where('asked_id', $user->id)->
							with(['asked', 'asking'])->
							whereNotNull('answer')->
							orderBy('updated_at', 'DESC')->
							get();
			return view( 'users.details', compact( 'user', 'questions' ) );
		}
		else {
			return view( 'errors.usernotfound' );
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(String $username, Request $request)
	{
		//
		$user = $this->validateUsername( $username );

		if( $user->id !== Auth::id() ) {

			$questions = 	Questions::where('asked_id', $user->id)->
										with(['asked', 'asking'])->
										whereNotNull('answer')->
										orderBy('updated_at', 'DESC')->
										get();

			$validatedData = $request->validate([
				'text' => 'required|min:3|max:255',
				'anonymous' => 'boolean',
				'email' => 'sometimes|required_with:password|email',
				'password' => 'sometimes|required_with:email|min:6'
			]);

			if( Auth::check() ) {
				$user_id = Auth::id();
			}
			else if ( isset( $request->email ) && isset( $request->password ) ) {

				$credentials = $request->only('email', 'password');

				if ( ! Auth::attempt($credentials) ) {
					// Didn't pass
					$request->session()->flash('login_error', 'Could not log you in. Please check your login information, and make sure it is correct!');

					return redirect( route('questions.index', $user->username ) )->withInput( $request->except('password') );
				}
				// If it did pass, do this
				if ( $user->id === Auth::id() ) {
					// Trying to ask yourself a question? Not allowed!
					$request->session()->flash('message', 'It looks like you are trying to ask yourself a question! Aw, are you that bored? :(');

					return redirect( route('questions.index', $user->username ) )->withInput( $request->except('password') );
				}

				$user_id = Auth::id();

			}
			else {
				// Didn't pass
				$request->session()->flash('login_error', 'There seems to be a technical issue! We will try to fix it ASAP.');

				return redirect( route('questions.index', $user->username ) );
			}

			$questions = new Questions;

			$questions->asked_id = $user->id;
			$questions->asking_id = $user_id;
			$questions->text = $request->text;
			$questions->anonymous = $request->input( 'anonymous', 0);

			$questions->save();

			$request->session()->flash('success', 'question');

			return redirect( route('questions.index', [$user->username]) );
		}
		else {
			// Trying to ask yourself a question? Not allowed!
			$request->session()->flash('message', 'It looks like you are trying to ask yourself a question! Aw, are you that bored? :(');

			return redirect( route('questions.index', $user->username ) )->withInput( $request->except('password') );
		}
	}

	public function answer( String $username, Request $request )
	{
		$user = $this->validateUsername( $username );

		$validatedData = $request->validate([
			'answer.*' => 'nullable|min:3|max:255',
		]);

		// dd( $request->question_id );
		$answer = false;

		for( $i = 0; $i < count( $request->input('question_id') ); $i++ )
		{
			// dump( '#' . $i . ' of ' .count( $request->input('question_id') ) .'- Answer: ' . $request->input('answer')[$i] ,
				//	'ID: ' . $request->input('question_id')[$i] );

			if( $request->input('answer')[$i] != '' && $request->input('answer')[$i] != null ) {
				$answer = Questions::where( 'id', $request->input('question_id')[$i] )->
					update( [ 'answer' => $request->input('answer')[$i] ] );

					// dump( 'Eloquent: ' . $answer );
			}
		}
		if ( $answer )
		{
			$request->session()->flash('success', 'answer');
		}
		else {
			$request->session()->flash('message', 'For some reason, the reply was not saved.');
		}
		return redirect( route('questions.index', [$user->username]) );

		// Trying to ask yourself a question? Not allowed!
		// $request->session()->flash('message', 'It looks like you are trying to access something you should not have access to. Sorry!');

		// return redirect( route('questions.index', $user->username ) )->withInput( $request->except('password') );

	}

	private function validateUsername( String $username )
	{
		if ( preg_match('/^[A-Za-z][A-Za-z0-9]*(?:[_-][A-Za-z0-9]+)*$/', $username) ) {
			$user = User::where('username', $username)->
					whereNotNull('confirmed_at')->
					first();

			if ( $user ) {
				return $user;
			}
			else
			{
				abort(404, 'Sorry, it looks like whoever you are looking for cannot be found!');
			}
		}
		else {
			abort(404, 'Sorry, it looks like whatever you are looking for cannot be found!');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Questions  $questions
	 * @return \Illuminate\Http\Response
	 */
	public function show(Questions $questions)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Questions  $questions
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Questions $questions)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Questions  $questions
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Questions $questions)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Questions  $questions
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Questions $questions)
	{
		//
	}
}
