<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AvatarController extends Controller
{
	//
	public function view( String $username )
	{
		$user = User::where( 'username', $username )->first();

		if( $user )
		{
			$class = 'text-blue fill-current rounded-full border-4 border-blue w-32 h-32';
			$avatar = 'anonymous-svg';

			if ( $user->avatar !== null && $user->avatar !== '' ) {
				$avatar = $user->avatar;
				// $avatarView = view('layouts.view-avatar', ['class' => $class, 'avatar' => $avatar] )->render();
			}
		}
		else
		{
			$avatar = 'anonymous-svg';
				// $avatarView = view('layouts.view-avatar', ['class' => $class, 'avatar' => $avatar] )->render();
		}

		return response()->
				view('layouts.view-avatar', ['class' => $class, 'avatar' => $avatar] )->
				withHeaders([
					'Content-Type' => 'image/svg; charset=utf-8',
					'Content-Disposition' => 'attachment; filename="avatar.svg"',
				]);
	}
}
