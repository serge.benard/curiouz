<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Questions extends Model
{
	use SoftDeletes;
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['deleted_at'];

	//
	protected $fillable = [
		'asked_id',
		'asking_id',
		'text',
		'answer',
		'anonymous',
	];
	public function asked()
	{
		return $this->belongsTo('App\User', 'asked_id', 'id' );
	}

	public function asking()
	{
		return $this->belongsTo( 'App\User', 'asking_id', 'id' );
	}
}
